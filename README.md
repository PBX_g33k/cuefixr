A small and simple Console application that corrects .cue files from japanese sources by 

1) converting the encoding from SHIFT-JIS to UTF-8 and 

2) correcting the FILE line by looking for the media file in directory.

#Example input:
```
REM GENRE Game
REM DATE 2013
REM DISCID 54060F08
REM COMMENT "ExactAudioCopy v0.99pb5"
PERFORMER "ƒCƒIƒVƒX"
TITLE "PUNK IT! TOUHOU! -IOSYS HITS PUNK COVERS-"
FILE "PUNK IT! TOUHOU! -IOSYS HITS PUNK COVERS-.wav" WAVE
TRACK 01 AUDIO
    TITLE "ƒ`ƒ‹ƒm‚Ìƒp[ƒtƒFƒNƒg‚³‚ñ‚·‚¤‹³Žº"
    PERFORMER "‚¿‚æ‚±"
    INDEX 01 00:00:00
  TRACK 02 AUDIO
    TITLE "ƒLƒƒƒvƒeƒ“Eƒ€ƒ‰ƒT‚ÌƒPƒcƒAƒ“ƒJ["
    PERFORMER "GŽO"
    INDEX 01 03:42:10
  TRACK 03 AUDIO
    TITLE "Club Ibuki in Break All"
    PERFORMER "‚¿‚æ‚±"
    INDEX 01 06:47:21
  TRACK 04 AUDIO
    TITLE "‚Ë‚±›Þ—‚ê‚¢‚Þ"
    PERFORMER "96"
    INDEX 01 10:11:30
  TRACK 05 AUDIO
    TITLE "s—ñ‚Ì‚Å‚«‚é‚¦[‚è‚ñf—ÃŠ"
    PERFORMER "‚ß‚ç‚Ý‚Û‚Á‚Õ"
    INDEX 01 13:18:40
  TRACK 06 AUDIO
    TITLE "ƒA[ƒeƒBƒtƒBƒVƒƒƒ‹Eƒ`ƒ‹ƒhƒŒƒ“"
    PERFORMER "96"
    INDEX 01 15:44:50
  TRACK 07 AUDIO
    TITLE "Phantasmagoria mystical expectation"
    PERFORMER "void"
    INDEX 01 19:29:24
  TRACK 08 AUDIO
    TITLE "Miracle‡Hinacle"
    PERFORMER "96"
    INDEX 01 22:33:33
```
#Example output:
```
REM GENRE Game
REM DATE 2013
REM DISCID 54060F08
REM COMMENT "ExactAudioCopy v0.99pb5"
PERFORMER "イオシス"
TITLE "PUNK IT! TOUHOU! -IOSYS HITS PUNK COVERS-"
FILE "PUNK IT! TOUHOU! -IOSYS HITS PUNK COVERS-.tta" WAVE
  TRACK 01 AUDIO
    TITLE "チルノのパーフェクトさんすう教室"
    PERFORMER "ちよこ"
    INDEX 01 00:00:00
  TRACK 02 AUDIO
    TITLE "キャプテン・ムラサのケツアンカー"
    PERFORMER "秀三"
    INDEX 01 03:42:10
  TRACK 03 AUDIO
    TITLE "Club Ibuki in Break All"
    PERFORMER "ちよこ"
    INDEX 01 06:47:21
  TRACK 04 AUDIO
    TITLE "ねこ巫女れいむ"
    PERFORMER "96"
    INDEX 01 10:11:30
  TRACK 05 AUDIO
    TITLE "行列のできるえーりん診療所"
    PERFORMER "めらみぽっぷ"
    INDEX 01 13:18:40
  TRACK 06 AUDIO
    TITLE "アーティフィシャル・チルドレン"
    PERFORMER "96"
    INDEX 01 15:44:50
  TRACK 07 AUDIO
    TITLE "Phantasmagoria mystical expectation"
    PERFORMER "void"
    INDEX 01 19:29:24
  TRACK 08 AUDIO
    TITLE "Miracle∞Hinacle"
    PERFORMER "96"
    INDEX 01 22:33:33
```
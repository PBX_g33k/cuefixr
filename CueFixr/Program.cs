﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace CueFixr
{   
    class Program
    {
        static string[] audioExtensions = { ".flac", ".tak", ".tta", ".mp3", ".aac", ".wav" };

        static void Main(string[] args)
        {
            //Debugger.Launch();
            Console.WriteLine("Number of command line parameters = {0}",
               args.Length);
            for (int i = 0; i < args.Length; i++)
            {
                string rawpath = args[i];
                string extension = Path.GetExtension(rawpath);
                string filename = Path.GetFileName(rawpath);
                string filenameNoExt = Path.GetFileNameWithoutExtension(rawpath);
                string pathroot = Path.GetDirectoryName(rawpath);

                if (extension != ".cue")
                {
                    Console.WriteLine("Input file is not a .cue file. .CUE expected, {0} given.", extension.ToUpper());
                    continue;
                }

                // Convert original file from SHIFT-JIS to UTF-8
                string convertedContent = convertFileContent(args[i]);
                Console.WriteLine("Converted file content from SHIFT-JIS to UTF-8");
                // Move original file to <filepath>.original
                File.Copy(args[i], args[i].ToString() + ".ori.cue",false);
                //Console.WriteLine("Moving {0} to {1}", args[i], args[i] + ".original");
                // Replace incorrect filename from convertedFile
                string correctFilename = getAudioFileFromFile(pathroot, filenameNoExt, filename);

                string correctContent = fixShit(convertedContent, correctFilename);

                Console.WriteLine("Correcting filename to {0}", correctFilename);
                //Replace incorrect filename from convertedContent
                saveFile(args[i], correctContent);

                Console.Write(convertedContent);
            }
            Console.WriteLine("My work here is done. Press the any key to exit.");
            //Console.ReadLine();
        }

        static string convertFileContent(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("shift-jis"));
            string ret = sr.ReadToEnd();
            sr.Close();
            return ret;
        }

        static void saveFile(string newpath, string content)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(content);

            using (StreamWriter sw = new StreamWriter(newpath, false, Encoding.UTF8 ) )
            {
                sw.Write(content);
                sw.Close();
            }
        }

        static string fixShit(string content, string correctExtension)
        {
            string ret = Regex.Replace( content, "FILE \\\"(.*)(.[a-zA-Z]*?)\\\" ", "FILE \"" + correctExtension + "\" ");
            return ret;
        }

        /// <summary>
        /// Get audio file from given cue file by looking in the directory.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        static string getAudioFileFromFile(string filepath, string filename, string fullFilename)
        {
            string[] files = Directory.GetFiles(filepath, filename + "*", SearchOption.TopDirectoryOnly);

            for(int i = 0; i < files.Length; i++)
            {
                if (audioExtensions.Contains(Path.GetExtension(files[i])))
                {
                    return Path.GetFileName(files[i]).ToString();
                }
            }

            return "";
        }

        /*static string fixShit(incorrectFileContent)
        {

        }*/

        /*static string[] getSimilarFiles(string source)
        {
            
        }*/
    }
}
